<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesHpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
          $table->increments('ServiceID');
          $table->integer('ParentServiceID')->nulleable();
          $table->string('Name',255);
          $table->text('Description')->nulleable();
          $table->string('Image',255)->nulleable();
          $table->string('Thumbnail',255)->nulleable();
          $table->integer('IconImageID')->nulleable();
          $table->integer('MainImageID')->nulleable();
          $table->string('CreatedByUserID',255)->nulleable();
          $table->string('ModifiedByUserID',255)->nulleable();
          $table->integer('IsHistorical');
          $table->integer('IsActive')->nulleable();
          $table->string('CheckListReportTypeCode',10)->nulleable();
          $table->integer('Status');
          $table->integer('OrderNumber')->nulleable();
          $table->string('ServiceKey',50)->nulleable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service');
    }
}
