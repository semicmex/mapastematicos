<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract', function (Blueprint $table) {
            $table->increments('ContractID');
            $table->integer('CompanyID');
            $table->text('ContractText');
            $table->date('ClientSignatureDate')->nulleable();
            $table->string('ClientSignatureUserID')->nulleable();
            $table->date('QCSignatureDate')->nulleable();
            $table->string('QCSignatureUserID')->nulleable();
            $table->date('ActivationDate')->nulleable();
            $table->date('DeactivationDate')->nulleable();
            $table->string('ContractName');
            $table->integer('PaymentTerms')->nulleable();
            $table->string('CreatedByUserID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contract');
    }
}
