<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratecategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RateCategoryCode',10);
            $table->string('Name',50);
            $table->text('Description');
            $table->integer('ServiceID')->nulleable();
            $table->boolean('FlatRate')->nulleable();
            $table->string('SubType',50)->nulleable();
            $table->boolean('IsActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ratecategory');
    }
}
