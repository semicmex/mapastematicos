<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CompanyUser', function (Blueprint $table) {
            $table->increments('id');
            $table->string('UserID');
            $table->integer('CompanyID')->nulleable();
            $table->string('RelationshipStatusCode',10);
            $table->string('FunctionalRoleCode',10);
            $table->string('Title',100)->nulleable();
            $table->string('TempCompanyName',100)->nulleable();
            $table->string('CreatedByUserID');
            $table->string('ModifiedByUserID')->nulleable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CompanyUser');
    }
}
