<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
          $table->increments('OrderID');
          $table->integer('ProjectID')->nulleable();
          $table->integer('QCOfficeID')->nulleable();
          $table->integer('CompanyID')->nulleable();
          $table->integer('ContractID')->nulleable();
          $table->date('OrderDate')->nulleable();
          $table->date('DueDate')->nulleable();
          $table->string('QCLeadUserID');
          $table->string('CustomerLeadUserID')->nulleable();
          $table->string('SpecialRequirements')->nulleable();
          $table->boolean('IsPublicData')->nulleable();
          $table->boolean('AllowThirdPartyAccess')->nulleable();
          $table->boolean('AllowResell')->nulleable();
          $table->string('DefaultPaymentCode')->nulleable();
          $table->boolean('IsVideoOnly')->nulleable();
          $table->boolean('IsRush')->nulleable();
          $table->boolean('IsVRCInvolved')->nulleable();
          $table->date('ExpectedVRCArrivalDate')->nulleable();
          $table->date('SentToCustomerDate')->nulleable();
          $table->string('SentToCustomerByUserID')->nulleable();
          $table->string('SentToCustomerTrackingNumber')->nulleable();
          $table->string('ClientReferenceNo')->nulleable();
          $table->string('CreatedByUserID')->nulleable();
          $table->string('ModifiedByUserID')->nulleable();
          $table->date('ModifiedDate')->nulleable();
          $table->boolean('CombineGroups')->nulleable();
          $table->boolean('ExcludeSetupFee')->nulleable();
          $table->date('NextContactDate')->nulleable();
          $table->string('ProjectName')->nulleable();
          $table->boolean('IsCancelled')->nulleable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
