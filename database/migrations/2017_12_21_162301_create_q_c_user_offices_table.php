<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQCUserOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('QCUserOffice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('UserID');
            $table->integer('QCOfficeID');
            $table->string('RelationshipStatusCode');
            $table->string('FunctionalRoleCode');
            $table->string('Title')->nulleable();
            $table->string('CreatedByUserID');
            $table->string('ModifiedByUserID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('QCUserOffice');
    }
}
