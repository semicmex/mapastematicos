<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carousel_items', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title',255);
          $table->text('description')->nulleable();
          $table->string('image')->nulleable();
          $table->string('video')->nulleable();
          $table->string('button')->nulleable();
          $table->string('link')->nulleable();
          $table->string('class')->nulleable();
          $table->string('user_id')->nulleable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carousel_items');
    }
}
