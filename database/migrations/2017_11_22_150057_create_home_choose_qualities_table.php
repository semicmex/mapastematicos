<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeChooseQualitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homechoosequality', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('index');
            $table->string('title',45);
            $table->text('description');
            $table->string('image',100);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homechoosequality');
    }
}
