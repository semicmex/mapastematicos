<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderservice', function (Blueprint $table) {
          $table->increments('OrderServiceID');
          $table->integer('OrderID')->unsigned();
           $table->foreign('OrderID')->references('OrderID')->on('Orders');
          $table->integer('ServiceID')->unsigned();
          $table->foreign('ServiceID')->references('ServiceID')->on('Service');
        

          $table->string('CreatedByUserID')->nulleable();
          $table->string('ModifiedByUserID')->nulleable();
          $table->date('ModifiedDate')->nulleable();
          $table->integer('GroupNumber')->nulleable();

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orderservice');
    }
}
