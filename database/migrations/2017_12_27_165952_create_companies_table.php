<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Company', function (Blueprint $table) {
            $table->increments('CompanyID');
            $table->integer('ParentCompanyID')->nulleable();
            $table->string('CompanyName',200);
            $table->string('AccountPayableEmail',256)->nulleable();
            $table->string('MailingAttention',200)->nulleable();
            $table->string('MailingAddress1',200);
            $table->string('MailingAddress2',200)->nulleable();
            $table->string('MailingCity',200);
            $table->char('MailingStateCode',2)->nulleable();
            $table->string('MailingZip',10);
            $table->string('BillingAttention',200)->nulleable();
            $table->string('BillingAddress1',200);
            $table->string('BillingAddress2',200)->nulleable();
            $table->string('BillingCity',200);
            $table->char('BillingStateCode',2)->nulleable();
            $table->string('BillingZip',10);
            $table->string('PhoneNumber',10)->nulleable();
            $table->string('FaxNumber',10)->nulleable();
            $table->string('DefaultPaymentTypeCode',10);
            $table->text('Comments')->nulleable();
            $table->string('CompanyStatusCode',10);
            $table->string('LegacyID',10)->nulleable();
            $table->string('CreatedByUserID');
            $table->string('ModifiedByUserID')->nulleable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Company');
    }
}
