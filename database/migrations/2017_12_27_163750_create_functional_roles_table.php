<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionalRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FunctionalRole', function (Blueprint $table) {
            $table->increments('id');
            $table->string('FunctionalRoleCode',10);
            $table->string('Name',50);
            $table->string('Description',100);
            $table->string('RoleType',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('FunctionalRole');
    }
}
