<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcofficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qcoffices', function (Blueprint $table) {
          $table->increments('QCOfficeID');
          $table->string('QCOfficeTypeCode');
          $table->string('OfficeName');
          $table->string('StreetAddress1');
          $table->string('StreetAddress2');
          $table->string('City');
          $table->string('StateCode');
          $table->string('ZipCode');
          $table->string('PhoneNumber');
          $table->string('FaxNumber');
          $table->string('CellNumber');
          $table->string('TollFreeNumber');
          $table->string('AlternateNumber');
          $table->string('Latitude');
          $table->string('Longitude');
          $table->string('CreatedByUserID');
          $table->string('CreatedDateTime');
          $table->string('ModifiedByUserID');
          $table->string('ModifiedDateTime');
          $table->string('QCOfficeCode');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qcoffices');
    }
}
