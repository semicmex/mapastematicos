<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipStatusTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RelationshipStatusType', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RelationshipStatusCode',10);
            $table->string('Name',50);
            $table->string('Description',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('RelationshipStatusType');
    }
}
