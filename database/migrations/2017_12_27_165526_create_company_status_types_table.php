<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStatusTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CompanyStatusType', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CompanyStatusCode',10);
            $table->string('Name',50);
            $table->string('Description',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CompanyStatusType');
    }
}
