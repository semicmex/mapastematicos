<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractdocument', function (Blueprint $table) {
            $table->increments('ContractDocumentID');
            $table->integer('ContractID');
            $table->string('FileTypeCode');
            $table->string('DocumentTypeCode');
            $table->string('FileName');
            $table->string('CreatedByUserID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contractdocument');
    }
}
