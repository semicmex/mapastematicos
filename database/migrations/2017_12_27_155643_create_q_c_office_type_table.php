<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQCOfficeTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('QCOfficeType', function (Blueprint $table) {
            $table->increments('id');
            $table->string('QCOfficeTypeCode',10);
            $table->string('Name',50);
            $table->string('Description',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('QCOfficeType');
    }
}
