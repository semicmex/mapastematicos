<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_info', function (Blueprint $table) {
            $table->increments('id');
            $table->text('company_desc');
            $table->string('company_image');
            $table->text('core_values_desc');
            $table->string('core_values_image');
            $table->text('organization_desc');
            $table->string('organization_image');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qc_info');
    }
}
