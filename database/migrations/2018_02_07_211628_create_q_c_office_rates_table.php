<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQCOfficeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qcofficerate', function (Blueprint $table) {
            $table->increments('QCOfficeRateID');
            $table->integer('QCOfficeID');
            $table->string('RateCategoryCode',10);
            $table->float('RateAmount');
            $table->boolean('IsActive');
            $table->text('Comments')->nulleable();
            $table->float('AdditionalRate')->nulleable();
            $table->float('ProjectSetupRate')->nulleable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qcofficerate');
    }
}
