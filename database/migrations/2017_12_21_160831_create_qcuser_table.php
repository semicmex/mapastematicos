<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User', function (Blueprint $table) {
            $table->increments('id');
            $table->string('UserID');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Position')->nulleable();
            $table->string('Fax')->nulleable();
            $table->string('Phone');
            $table->string('Email');
            $table->string('Username');
            $table->integer('IsAnonymous');
            $table->string('Password');
            $table->integer('PasswordFormat');
            $table->string('PasswordSalt');
            $table->string('PasswordQuestion')->nulleable();
            $table->string('PasswordAnswer')->nulleable();
            $table->boolean('IsApproved');
            $table->boolean('IsLockedOut');
            $table->date('LastLoginDate')->nulleable();
            $table->date('LastPasswordChangeDate')->nulleable();
            $table->date('LastLockedoutDate')->nulleable();
            $table->integer('FailedPasswordAttemptCount');
            $table->integer('FailedPasswordAnswerAttemptCount');
            $table->string('Photo')->nulleable();
            $table->text('Description')->nulleable();
            $table->integer('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('User');
    }
}
