<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramo extends Model
{
  protected $table = 'tramo';


  protected $fillable = ['CLAVE',
                    'SENTIDO',
                    'CARRIL',
                    'DE_CAD',
                    'A_CAD',
                    'LATITUD_INI',
                    'LONGITUD_INI',
                    'LATITUD_FIN',
                    'LONGITUD_FIN',
                    'IRI',
                    'PROMEDIO_IRI',
                    'PR',
                    'PROMEDIO_PR',
                    'MAC',
                    'PROMEDIO_MAC',
                    'DET',
                    'DET_PORCENT',
                    'PROMEDIO_DET'];
 
}
