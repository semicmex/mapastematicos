<?php

namespace App\Http\Controllers;

use DB;
use Hash;

use Illuminate\Http\Request;

use App\Tramo;
use App\Anexo;
use Illuminate\Support\Facades\Response;

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');
class TramoController extends Controller
{
  public function processAll(){
    echo "0001";
    // get list of tramos 
    //AQUI SE COLOCAN LOS TRAMOS LOS CUELES SE QUIEREN SACAR SU PDF
     $tramos=array("BC-211-04", "BC-212-01", "BC-212-02", "BC-221-01", "BC-238-01", "BC-238-02", "BC-238-03", "BC-238-04", "BC-238-05", "BC-241-01", "BC-241-02", "BC-244-01", "BC-244-02", "BC-244-03", "BC-244-04", "BC-245-01", "BC-269-01", "BC-272-01", "BC-272-02", "BC-272-03", "BC-272-04", "BC-272-05", "BC-272-06", "BC-273-01", "BC-273-02", "BC-273-03", "BC-274-04", "BC-274-05", "BC-274-06", "BC-276-01", "BC-276-02");

    foreach ($tramos as  $vt) {
      //verificar si la clave existe en la tabla de ANEXO para extraer el nombre de la carretera 
      $anexo=Anexo::where('CLAVE',$vt)->count();
        
      //validate anexo
      if ($anexo>0) {
        echo $vt. " ----1 <br />\n";
        // get sentidos
        $sentidos=Tramo::where('CLAVE',$vt)->groupBy('SENTIDO')->get(['SENTIDO']);
        echo "---01";
        foreach ($sentidos as $sentido ) {
          //echo $sentido->SENTIDO."</br>\n";
          //get CArriles
          $carriles=Tramo::where('CLAVE',$vt)
                    ->where('SENTIDO',$sentido->SENTIDO)
                    ->groupBy('CARRIL')
                    ->get(['CARRIL']);
          // foreach ($carriles as $carril) {
          //             echo "--C: ".$carril."</br>\n" ;
          //           }   
          //calcula longitud
                    
          $longitud=Tramo::where('CLAVE',$vt)
                    ->where('SENTIDO',$sentido->SENTIDO)
                    ->orderBy('NUM','ASC')
                    ->get(['DE_CAD','A_CAD','LATITUD_INI','LONGITUD_INI','num']);
         
                    //calcular la longitus desde DB
         $longitud_count=Tramo::where('CLAVE',$vt)
                    ->where('SENTIDO',$sentido->SENTIDO)
                    ->count();            
          $de_cad=$longitud[0]->DE_CAD;
          $a_cad=$longitud[$longitud_count-1]->DE_CAD;     
          
          $lat_ini=$longitud[0]->LATITUD_INI;
          $lng_ini=$longitud[0]->LONGITUD_INI;

          $lat_fin=$longitud[$longitud_count-1]->LATITUD_INI;
          $lng_fin=$longitud[$longitud_count-1]->LONGITUD_INI;

          echo "2";
          $de_cad_str=str_replace("+", "",$de_cad);
          $a_cad_str=str_replace("+", "",$a_cad);
          $longitud_total=intval($a_cad_str)-intval($de_cad_str);
          $longitud_to=($longitud_total/1000);
          /// genera el pdf
          echo "3";
          $download=$this->getData($vt,$sentido->SENTIDO,$carriles[0]->CARRIL,$de_cad,$a_cad,$lat_ini,$lng_ini,$lat_fin,$lng_fin,$longitud_to);
          echo "4";
        }
        
      }else{
       echo "La carretera ".$vt." no tiene un Nombre registrado </br>\n";  
      }

      
      
    }
  }
  public function createPDFbyTramos(Request $request){
        $tramo=$request->input("tramo");
        $sentido=$request->input("sentido");


        $fileName= $tramo.'-S'.$sentido.'.pdf';
            $estimateName=$fileName;

       $carriles=Tramo::where('CLAVE',$tramo)
                    ->where('SENTIDO',$sentido)
                    ->groupBy('CARRIL')
                    ->get(['CARRIL']);

                  //calcula la longitud desde BD

        $longitud=Tramo::where('CLAVE',$tramo)
                    ->where('SENTIDO',$sentido)
                    ->orderBy('NUM','ASC')
                    ->get(['DE_CAD','A_CAD','LATITUD_INI','LONGITUD_INI','num']);

                    //obtener el total de registros
         $longitud_count=Tramo::where('CLAVE',$tramo)
                    ->where('SENTIDO',$sentido)
                    ->count();   

          //0 es el primer registro de cad
          $de_cad=$longitud[0]->DE_CAD;
          //ultimom registro  total de registro menos 1 
          $a_cad=$longitud[$longitud_count-1]->DE_CAD;     
          
          $lat_ini=$longitud[0]->LATITUD_INI;
          $lng_ini=$longitud[0]->LONGITUD_INI;

          $lat_fin=$longitud[$longitud_count-1]->LATITUD_INI;
          $lng_fin=$longitud[$longitud_count-1]->LONGITUD_INI;

          
          $de_cad_str=str_replace("+", "",$de_cad);
          $a_cad_str=str_replace("+", "",$a_cad);
          
          $longitud_total=intval($a_cad_str)-intval($de_cad_str);

          //agregar formato##+###
          

          
          $longitud_to=($longitud_total/1000);
          //return  array($de_cad,$a_cad,$lat_ini,$lng_ini,$lat_fin,$lng_fin,$longitud_to);              
       //genera el pdf
        $download=$this->getData($tramo,$sentido,$carriles[0]->CARRIL,$de_cad,$a_cad,$lat_ini,$lng_ini,$lat_fin,$lng_fin,$longitud_to);
        
          return $download->download($estimateName);
        //return $download;

  }
   public function getData($tramo,$sentido,$carril,$de_cad,$a_cad,$lat_ini,$lng_ini,$lat_fin,$lng_fin, $longitud_to){
        
        // $all = Tramo::where('CLAVE',$tramo)
        // ->where('SENTIDO',$sentido)
        // ->limit(10)
        // ->get(); 
    $carril_main=$carril;

         $anexo = Anexo::where('CLAVE',$tramo)
                
               ->get();
               foreach ($anexo as $road)
              {
                  echo $road->name;
              }
        //return Response::json($anexo);
        $result_data=$this->getTramosByClave($tramo,$sentido);
        //echo "--------------------------";
        //var_dump($result_data[5][0]);
       //return $result_data;

          $view =  \View::make('pdf.reporte', compact('result_data','anexo','sentido','tramo','carril_main','de_cad','a_cad','lat_ini','lng_ini','lat_fin','lng_fin','longitud_to'))->render();
            
             $pdf = \App::make('snappy.pdf.wrapper')
              ->setOrientation('landscape')
              ->setOption('margin-bottom', '0mm');
             
              $pdf->setPaper("letter");
              $pdf->loadHTML($view);
             //return $pdf->inline();
              $fileName= $tramo.'-S'.$sentido.'.pdf';
              $path="C:/reporteMapas/";
             $pdf->save($path.$fileName);
             echo $path.$fileName;

            return $pdf;
        
   }
   public function getTramosByClave($tramo,$sentido){
        // $tramo=$request->input("tramo");
        // $sentido=$request->input("sentido");
        $carriles=Tramo::where('CLAVE',$tramo)
        ->where('SENTIDO',$sentido)
      
        ->groupBy('CARRIL')
        ->get(['CARRIL']); 
        //return Response::json($carriles);
        $array=array();
        foreach ($carriles as $key => $value) {
           $all = Tramo::select(DB::raw('SUM(CASE WHEN PROMEDIO_IRI="No Aceptable" THEN 1 ELSE 0 END) as iri_noa,
                SUM(CASE WHEN PROMEDIO_IRI="Aceptable" THEN 1 ELSE 0 END) as iri_a, 
                SUM(CASE WHEN PROMEDIO_IRI="Bueno" THEN 1 ELSE 0 END) as iri_b, 
                count(PROMEDIO_IRI) as total_iri,
                SUM(CASE WHEN PROMEDIO_IRI="No Aceptable" THEN 1 ELSE 0 END) as iri_noa,
                SUM(CASE WHEN PROMEDIO_IRI="Aceptable" THEN 1 ELSE 0 END) as iri_a, 
                SUM(CASE WHEN PROMEDIO_IRI="Bueno" THEN 1 ELSE 0 END) as iri_b, 
                count(PROMEDIO_IRI) as total_iri ,

                SUM(CASE WHEN PROMEDIO_PR="No Aceptable" THEN 1 ELSE 0 END) as pr_noa,
                SUM(CASE WHEN PROMEDIO_PR="Aceptable" THEN 1 ELSE 0 END) as pr_a, 
                SUM(CASE WHEN PROMEDIO_PR="Bueno" THEN 1 ELSE 0 END) as pr_b, 
                count(PROMEDIO_PR) as total_pr ,

                SUM(CASE WHEN PROMEDIO_MAC="No Aceptable" THEN 1 ELSE 0 END) as mac_noa,
                SUM(CASE WHEN PROMEDIO_MAC="Aceptable" THEN 1 ELSE 0 END) as mac_a, 
                SUM(CASE WHEN PROMEDIO_MAC="Bueno" THEN 1 ELSE 0 END) as mac_b, 
                count(PROMEDIO_MAC) as total_mac ,

                SUM(CASE WHEN PROMEDIO_DET="No Aceptable" THEN 1 ELSE 0 END) as det_noa,
                SUM(CASE WHEN PROMEDIO_DET="Aceptable" THEN 1 ELSE 0 END) as det_a, 
                SUM(CASE WHEN PROMEDIO_DET="Bueno" THEN 1 ELSE 0 END) as det_b, 
                count(PROMEDIO_DET) as total_det '))
        ->where('CLAVE',$tramo)
        ->where('SENTIDO',$sentido)
        ->where('CARRIL',$value['CARRIL'])
        ->limit(10)
        ->get(); 
        $array[$value['CARRIL']]=$all ;
        }

        // $all = Tramo::where('CLAVE',$tramo)
        // ->where('SENTIDO',$sentido)
        // ->where('CARRIL',$carril)
        // ->limit(10)
        // ->get(); 

        //   return Response::json(array('status'=>0, 'data'=>$all));

     foreach ($array as $k => $v) {


              //calcula IRI
              $total_iri=$array[$k][0]['iri_noa']+$array[$k][0]['iri_a']+$array[$k][0]['iri_b'];
             $array[$k][0]['total_iri']=$total_iri;
             if ($total_iri==0) {
                  $array[$k][0]['iri_noa']=array(0,0);
                  $array[$k][0]['iri_a']=array(0,0);
                  $array[$k][0]['iri_b']=array(0,0);
               
             }else{
                  $iri_noa=intval($array[$k][0]['iri_noa'])/$array[$k][0]['total_iri']*100;
                  $iri_a=intval($array[$k][0]['iri_a'])/$array[$k][0]['total_iri']*100;
                  $iri_b=intval($array[$k][0]['iri_b'])/$array[$k][0]['total_iri']*100;

                 $resultIri=$this->getvalorMaximo($iri_noa,$iri_a,$iri_b);

                  $array[$k][0]['iri_noa']=$resultIri[0];
                  $array[$k][0]['iri_a']=$resultIri[1];
                  $array[$k][0]['iri_b']=$resultIri[2];

             }
             
             
             

              //calcula PR
              $total_pr=$array[$k][0]['pr_noa']+$array[$k][0]['pr_a']+$array[$k][0]['pr_b'];
              $array[$k][0]['total_pr']= $total_pr;
              if ($total_pr==0) {
                $array[$k][0]['pr_noa']=array(0,0);
                  $array[$k][0]['pr_a']=array(0,0);
                  $array[$k][0]['pr_b']=array(0,0);
                
              }else{
                  $pr_noa=intval($array[$k][0]['pr_noa'])/$array[$k][0]['total_pr']*100;
                  $pr_a=intval($array[$k][0]['pr_a'])/$array[$k][0]['total_pr']*100;
                  $pr_b=intval($array[$k][0]['pr_b'])/$array[$k][0]['total_pr']*100;

                 $resultPr=$this->getvalorMaximo($pr_noa,$pr_a,$pr_b);

                 
                  $array[$k][0]['pr_noa']=$resultPr[0];
                  $array[$k][0]['pr_a']=$resultPr[1];
                  $array[$k][0]['pr_b']=$resultPr[2];

              }

              


              //endcalcula PR
              //calcula mac$
              $total_mac=$array[$k][0]['mac_noa']+$array[$k][0]['mac_a']+$array[$k][0]['mac_b'];
              if ($total_mac==0) {
                  $array[$k][0]['mac_noa']=array(0,0);
                  $array[$k][0]['mac_a']=array(0,0);
                  $array[$k][0]['mac_b']=array(0,0);
                
              }else{
                  $array[$k][0]['total_mac']=$total_mac;
                  $mac_noa=intval($array[$k][0]['mac_noa'])/$array[$k][0]['total_mac']*100;
                  $mac_a=intval($array[$k][0]['mac_a'])/$array[$k][0]['total_mac']*100;
                  $mac_b=intval($array[$k][0]['mac_b'])/$array[$k][0]['total_mac']*100;

                 $resultMac=$this->getvalorMaximo($mac_noa,$mac_a,$mac_b);

                  $array[$k][0]['mac_noa']=$resultMac[0];
                  $array[$k][0]['mac_a']=$resultMac[1];
                  $array[$k][0]['mac_b']=$resultMac[2];

              }
              

              // $array[$k][0]['mac_noa']=$mac_noa;
              // $array[$k][0]['mac_a']=$mac_a;
              // $array[$k][0]['mac_b']=$mac_b;
              

              
              //endcalcula mac
               $array[$k][0]['total_det']=$array[$k][0]['det_noa']+$array[$k][0]['det_a']+$array[$k][0]['det_b'];


       }

    return $array; 
        
   }
   public function getDecimals($n){
              $whole = floor($n);      // 1
              $fraction = $n - $whole; // .25
              return array($whole,$fraction);

   }
   public function getvalorMaximo($noa,$a,$b){
              $noa1=round($noa);
              $a1=round($a);
              $b1=round($b);
              if (($noa1+$a1+$b1)==100) {
                $decimalesNoa=array($noa1,0);
                $decimalesA=array($a1,0);
                $decimalesB=array($b1,0);
              }else{
                 $decimalesNoa=$this->getDecimals(round($noa,4));
                  $decimalesA=$this->getDecimals(round($a,4));
                  $decimalesB=$this->getDecimals(round($b,4));
                  if ($decimalesNoa[0]+$decimalesA[0]+ $decimalesB[0]==100) {
                    # code...
                  }else{

                     if($decimalesNoa[1] > $decimalesA[1]){

                        if($decimalesNoa[1] > $decimalesB[1]){
                           $decimalesNoa[0]++; 
                          }
                        }

                      if($decimalesA[1] > $decimalesNoa[1]){

                        if($decimalesA[1] > $decimalesB[1]){
                         $decimalesA[0]++; 
                        }
                      }

                      if($decimalesB[1] > $decimalesNoa[1]){

                        if($decimalesB[1] > $decimalesA[1]){
                         $decimalesB[0]++; 
                        }
                      }    
                     
                  }
              }

              return array($decimalesNoa,$decimalesA,$decimalesB);       

   }

 
 
}
     //$tramos=Tramo::groupBy('CLAVE')->get(['CLAVE']);
