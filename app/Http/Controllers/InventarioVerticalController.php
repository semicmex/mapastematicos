<?php

namespace App\Http\Controllers;

use DB;
use Hash;

use Illuminate\Http\Request;

use App\Tramo;
use App\Anexo;
use App\Captura;
use Illuminate\Support\Facades\Response;
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

class InventarioVerticalController extends Controller
{
   public function senalamientoPDF(Request $request){

$skip = $request->input('skip');//1
$limit = $request->input('limit'); //1000 

    $row = 1;
    
      if (($handle = fopen("D:/senalamiento.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          if (  $row>=$skip && $row<=$limit) {
            
         
          $num = count($data);
          echo "<p> $num fields in line $row: <br /></p>\n";
         
          for ($c=0; $c < $num; $c++) {
              echo $data[$c] . "<br />\n";
          }
           $path="D:/senalamiento/";
           //checar si carpeta existe si no crearla
           $carretera=$path.$data[0]."/";
           
          if (!file_exists($carretera)) {
              mkdir($carretera, 0777, true);
          }

          $fileName= $data[27].'.pdf';
          
           echo $fileName."<br /></p>\n";
           $image = Captura::find($data[27]);
          
          $img=$image->image;
          echo $img."<br /></p>\n";
          if ($img=="") {
            $img="http://localhost/logos/white.png";
          }
          $view =  \View::make('pdf.senalamiento', compact('data','img','row'))->render();
          

            //$index=str_pad($row,  3, "0", STR_PAD_LEFT);
            
             
            $estimateName=$fileName;
       
             $pdf = \App::make('snappy.pdf.wrapper')
              ->setOrientation('portrait')
              ->setOption('margin-bottom', '0mm');
             
              $pdf->setPaper("letter");
             $pdf->loadHTML($view);
             //return $pdf->inline();

             //echo $path.$fileName."<br /></p>\n";;
//             $path="D:/senalamiento/";

             $pdf->save($carretera.$fileName);
             echo $path.$fileName;

            //return $pdf->download($estimateName);
              }
              $row++;
        }
        fclose($handle);
      }
       
     
        
   }

   public function contencionPDF(Request $request){
    $row = 1;
      if (($handle = fopen("E:/2019/Exportacion_inventario/contencion5.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          $num = count($data);
          echo "<p> $num fields in line $row: <br /></p>\n";
         
          for ($c=0; $c < $num; $c++) {
              echo $data[$c] . "<br />\n";
          }
          $fileName= $row.'_contencion_'.$data[0].'.pdf';
          
          echo $fileName."<br /></p>\n";
          $image = Captura::find($data[27]);;
          
          $img=$image->image;
          echo $img."<br /></p>\n";
          $view =  \View::make('pdf.contencion', compact('data','img','row'))->render();

          $row++;
            //$index=str_pad($row,  3, "0", STR_PAD_LEFT);
            
            
            $estimateName=$fileName;
       
             $pdf = \App::make('snappy.pdf.wrapper')
              ->setOrientation('portrait')
              ->setOption('margin-bottom', '0mm');
             
              $pdf->setPaper("letter");
             $pdf->loadHTML($view);
             //return $pdf->inline();

             //echo $path.$fileName."<br /></p>\n";;
             $path="E:/inventario_vertical/pdf/";

             $pdf->save($path.$fileName);
             echo $path.$fileName;

            //return $pdf->download($estimateName);
        }
        fclose($handle);
      }
       
     
        
   }

   public function cortesPDF(Request $request){


   
    $row = 1;
      if (($handle = fopen("D:/cortesCompleto.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          $num = count($data);
          echo "<p> $num fields in line $row: <br /></p>\n";
         
          for ($c=0; $c < $num; $c++) {
              echo $data[$c] . "<br />\n";
          }
          $path="D:/cortes/";
           //checar si carpeta existe si no crearla
           $carretera=$path.$data[0]."/";
           $carreteraCortes=$carretera."cortes/";
           
          if (!file_exists($carretera)) {
              mkdir($carretera, 0777, true);
          }
          if (!file_exists($carreteraCortes)) {
              mkdir($carreteraCortes, 0777, true);
          }

          $fileName= $row."_".$data[27].'.pdf';
         // $fileName= $row.'_cortes_'.$data[0].'.pdf';
          
           echo $fileName."<br /></p>\n";;
            $image = Captura::find($data[27]);

              $screenshot_inicial="http://localhost/logos/white.png";
              $screenshot_intermedia="http://localhost/logos/white.png";
              $screenshot_final="http://localhost/logos/white.png";
            
            if ($image) {

              $screenshot_inicial=$img=$image->screenshot_inicial;
              if ($screenshot_inicial=="") {
                $screenshot_inicial="http://localhost/logos/white.png";
              }
              $screenshot_intermedia=$img=$image->screenshot_intermedia;
              if ($screenshot_intermedia=="") {
                $screenshot_intermedia="http://localhost/logos/white.png";
              }

              $screenshot_final=$img=$image->screenshot_final;
              if ($screenshot_final=="") {
                $screenshot_final="http://localhost/logos/white.png";
              }
              
            }
          
          
          
          echo $screenshot_inicial."<br /></p>\n";;
          $view =  \View::make('pdf.cortes', compact('data','img','row','screenshot_inicial','screenshot_intermedia','screenshot_final'))->render();
            $row++;

            //$index=str_pad($row,  3, "0", STR_PAD_LEFT);
            
            
            $estimateName=$fileName;
       
             $pdf = \App::make('snappy.pdf.wrapper')
              ->setOrientation('portrait')
              ->setOption('margin-bottom', '0mm');
             
              $pdf->setPaper("letter");
             $pdf->loadHTML($view);
             //return $pdf->inline();

             //echo $path.$fileName."<br /></p>\n";;
             //$path="E:/inventario_vertical/pdf/";

             $pdf->save($carreteraCortes.$fileName);
             echo $path.$fileName;

            //return $pdf->download($estimateName);
        }
        fclose($handle);
      }
       
     
        
   }
   public function AnunciosPDF(Request $request){


   
    $row = 1;
      if (($handle = fopen("E:/2019/Exportacion_inventario/anuncios.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          $num = count($data);
          echo "<p> $num fields in line $row: <br /></p>\n";
         
          for ($c=0; $c < $num; $c++) {
              echo $data[$c] . "<br />\n";
          }
          $fileName= $row.'_anuncios_'.$data[0].'.pdf';
          
           echo $fileName."<br /></p>\n";;
            $image = Captura::find($data[27]);;
          

          
          $screenshot_inicial=$img=$image->screenshot_inicial;
          
          echo $screenshot_inicial."<br /></p>\n";;
          $view =  \View::make('pdf.anuncios', compact('data','img','row','screenshot_inicial'))->render();
            $row++;

            //$index=str_pad($row,  3, "0", STR_PAD_LEFT);
            
            
            $estimateName=$fileName;
       
             $pdf = \App::make('snappy.pdf.wrapper')
              ->setOrientation('portrait')
              ->setOption('margin-bottom', '0mm');
             
              $pdf->setPaper("letter");
             $pdf->loadHTML($view);
             //return $pdf->inline();

             //echo $path.$fileName."<br /></p>\n";;
             $path="E:/inventario_vertical/pdf/";

             $pdf->save($path.$fileName);
             echo $path.$fileName;

            //return $pdf->download($estimateName);
        }
        fclose($handle);
      }
       
     
        
   }
   
 
}
