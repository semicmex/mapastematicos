<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class Both
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            /* get token */
            if (!$request->header('authorization')) {
                return response()->json(array('error'=> 'No data available'));
            }

            $authorization = $request->header('authorization');
            $authorization = str_replace("Bearer ", "", $authorization);

            if ($authorization) {
                /* get token data */
                $token = JWT::decode($authorization, 'BbW2Hvl1UVHGqhteCjERyNpbMjNux2TB', ['HS256']);
                if( !$token->userId){
                    return response()->json(array('error'=> 'No data available'));
                }

                return $next($request);    
            }

            return response()->json(array('error'=> 'No data available'));

        } catch(Exception $e){
            app()->make("lern")->record($e);
            return Response::json(array('Sorry, there was an error, please contact your support personnel for assistance.'));
        }

    }
}
