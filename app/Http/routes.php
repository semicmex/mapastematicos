<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


  Route::resource('download_PDF','TramoController@createPDFbyTramos');
  Route::resource('getTramosByClave','TramoController@getTramosByClave');

  
Route::get('runParkingPDF', 'ParkingController@runParkingPDF');

Route::get('processAll', 'TramoController@processAll');

Route::get('processSenalamiento', 'InventarioVerticalController@senalamientoPDF');

Route::get('processContencion', 'InventarioVerticalController@contencionPDF');

Route::get('processCortes', 'InventarioVerticalController@cortesPDF');
Route::get('processAnuncios', 'InventarioVerticalController@AnunciosPDF');



// Route::get('runParkingPDF', function () {
//     return "runParkingPDF";
// });