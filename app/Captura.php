<?php
namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Captura extends Eloquent
{
    protected $connection = 'mongodb';
    public $collection = 'capturas';
    public $attributes;
}

