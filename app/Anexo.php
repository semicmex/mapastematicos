<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anexo extends Model
{
  protected $table = 'anexo';


  protected $fillable = ['no',
                    'clave',
                    'estado',
                    'concesionaria',
                    'nombre_carretera',
                    'de',
                    'a',
                    'carriles_sentido',
                    'no_carriles',
                    'sentido',
                    'longitud',
                    'longitud_evaluacion',
                    'inicial',
                    'final',
                    'latitud_ini',
                    'longitud_ini',
                    'latitud_fin',
                    'longitud_fin'];
 
}
